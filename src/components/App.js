import React from 'react';
import SearchBar from './SearchBar';
import VideoDetail from './VideoDetail';
import VideoList from './VideoList';
import youtube from '../apis/YouTube';

const KEY = 'AIzaSyCfhjXKkGeRICReX3J4JrmMVD9VScJYph8';

class App extends React.Component {

  state = { videos: [] };
  componentDidMount() {
    this.onSearchSubmit('buildings');

  }

  onSearchSubmit = async search => {
    const response = await youtube.get(
      '/search',
      {
        params: {
          q: search,
          part: "snippet",
          maxResults: 25,
          key: KEY
        }
      }
    );
    this.setState({
      videos: response.data.items,
      selectedVideo: response.data.items[0]
     });
  }

  onVideoSelect = (video) =>{
    this.setState({ selectedVideo: video });
  };

  render(){  
    return(
      <div className="ui container" >
          <SearchBar onFormSubmit={this.onSearchSubmit} />
          Found {this.state.videos.length } videos.
          <div className="ui grid">
            <div className="ui row">
              <div className="eleven wide column" >
                <VideoDetail video={this.state.selectedVideo} />
              </div>
              <div className="five wide column" >
                <VideoList onVideoSelect={this.onVideoSelect} videos={this.state.videos} />
              </div>
            </div>
          </div>                   
      </div> 
    );    
  }
}

export default App;